import React, { Component } from "react"
import axios from "axios"
import genders from "./genders"

export class RegisterForm extends Component {
  state = {
    nickname: "",
    email: "",
    gender: "",
    password: "",
    confirmPass: ""
  }

  handleChangeUserN = event => {
    this.setState({ nickname: event.target.value })
  }
  handleChangeEmail = event => {
    this.setState({ email: event.target.value })
  }
  handleChangeGender = event => {
    this.setState({ gender: event.target.value })
  }
  handleChangePassword = event => {
    this.setState({ password: event.target.value })
  }
  handleChangeConfPassword = event => {
    this.setState({ confirmPass: event.target.value })
  }

  register = () => {
    axios.post("http://localhost:3001/users", {
      nickname: this.state.nickname,
      email: this.state.email,
      gender: this.state.gender,
      password: this.state.password
    })
  }

  render() {
    return (
      <div className="App">
        <label>
          UserName:
          <input
            id="UserName"
            type="Text"
            placeholder="UserName"
            value={this.state.nickname}
            onChange={this.handleChangeUserN}
          />
        </label>
        <label>
          Email:
          <input
            id="Email"
            type="Text"
            placeholder="Email"
            value={this.state.email}
            onChange={this.handleChangeEmail}
          />
        </label>
        <label>
          Genders
          <input
            type="range"
            min="0"
            max="31"
            value={this.state.gender}
            class="slider"
            id="myRange"
            onChange={this.handleChangeGender}
          />
          <div>{genders[this.state.gender]}</div>
        </label>
        <label>
          Password:
          <input
            id="Password"
            type="Text"
            placeholder="Password"
            value={this.state.password}
            onChange={this.handleChangePassword}
          />
        </label>
        <label>
          ConfirmPassword:
          <input
            id="Password"
            type="Text"
            placeholder="Password"
            value={this.state.confirmPass}
            onChange={this.handleChangeConfPassword}
          />
        </label>
        <button onClick={() => this.register()}>Register</button>
      </div>
    )
  }
}
