import React, { Component } from "react"
import "./App.css"
import { LoginForm } from "./LoginForm"
import { AddMovie } from "./addmoviePage"
import { RegisterForm } from "./registerForm"
import { BrowserRouter as Router, Route } from "react-router-dom"
import { Header } from "./Header"
import AppContext from "./AppContext"
import { RenderMovies } from "./renderMovies"

class App extends Component {
  state = {
    user: {}
  }

  setUser = user => {
    console.log(user)
    this.setState({ user })
  }

  render() {
    console.log(this.state)
    return (
      <Router>
        <AppContext.Provider
          value={{ state: this.state, setUser: this.setUser }}
        >
          <Header />
          <div className="App">
            <Route path="/login" exact component={LoginForm} />
            <Route path="/register" exact component={RegisterForm} />
            <Route path="/movies" exact component={AddMovie} />
            <Route path="/rendermovies" exact component={RenderMovies} />
          </div>
        </AppContext.Provider>
      </Router>
    )
  }
}

export default App
