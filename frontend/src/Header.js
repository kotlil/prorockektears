import React, { Component } from "react"
import { Link } from "react-router-dom"
import AppContext from "./AppContext"

export class Header extends Component {
  static contextType = AppContext

  logOut = e => {
    this.context.setUser({})
  }

  render() {
    let loggedIn = this.context.state.user.nickname
    if (loggedIn) {
      return (
        <div>
          <span>{loggedIn}</span>
          <button onClick={this.logOut}>Log out</button>
        </div>
      )
    } else {
      return (
        <div>
          <button>
            <Link to="/login">Log in</Link>
          </button>
        </div>
      )
    }
  }
}
