import React, { Component } from "react"
import axios from "axios"

export class AddMovie extends Component {
  state = {
    title: "",
    imdbId: ""
  }

  handleChangetitle = event => {
    this.setState({ title: event.target.value })
  }
  handleChangeId = event => {
    this.setState({ imdbId: event.target.value })
  }

  AddMovie = () => {
    axios.post("http://localhost:3001/movies", {
      title: this.state.title,
      imdbId: this.state.imdbId
    })
  }

  render() {
    return (
      <div className="App">
        <form onSubmit={() => this.AddMovie()}>
          <label>
            Title:
            <input
              id="Title"
              type="Text"
              placeholder="Title"
              value={this.state.title}
              onChange={this.handleChangetitle}
              required
            />
          </label>
          <label>
            ImdbId:
            <input
              id="ImdbId"
              type="Text"
              placeholder="ImdbId"
              value={this.state.imdbId}
              onChange={this.handleChangeId}
              required
            />
          </label>
          <input type="submit" value="Add movie" />
        </form>
      </div>
    )
  }
}
