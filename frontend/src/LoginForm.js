import React, { Component } from "react"
import axios from "axios"
import AppContext from "./AppContext"
import { Link } from "react-router-dom"

export class LoginForm extends Component {
  static contextType = AppContext

  state = { nickname: "", password: "" }

  handleChangeEmail = event => {
    this.setState({ nickname: event.target.value })
  }
  handleChangePassword = event => {
    this.setState({ password: event.target.value })
  }

  login = event => {
    event.preventDefault()
    console.log(this.state)
    console.log(this.context)
    axios
      .post(
        "http://localhost:3001/users/login",
        {
          nickname: this.state.nickname,
          password: this.state.password
        },
        { withCredentials: true }
      )
      .then(response => {
        console.log(response.data)
        this.context.setUser(response.data)
        this.props.history.push("/register")
      })
  }

  render() {
    return (
      <div className="App">
        <form onSubmit={this.login}>
          <label>
            Email:
            <input
              id="Email"
              type="Text"
              placeholder="Nickname"
              value={this.state.nickname}
              onChange={this.handleChangeEmail}
              required
            />
          </label>
          <label>
            Password:
            <input
              id="Password"
              type="password"
              placeholder="Password"
              value={this.state.password}
              onChange={this.handleChangePassword}
              required
            />
          </label>
          <input type="submit" value="Login" />
          <button>
            <Link to="/register">Register</Link>
          </button>
        </form>
      </div>
    )
  }
}
