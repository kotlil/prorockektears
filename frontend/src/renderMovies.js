import React, { Component } from "react"
import axios from "axios"

export class RenderMovies extends Component {
  state = {
    movies: []
  }
  componentDidMount() {
    axios
      .get("http://localhost:3001/movies")
      .then(response => {
        const films = response.data
        films.forEach(film => {
          if (!film.imdbId) {
            return
          }
          axios
            .get(`http://www.omdbapi.com/?apikey=50ca5fb1&i=${film.imdbId}`)
            .then(response => {
              const filmData = {
                title: film.title,
                imdbId: film.imdbId,
                year: response.data.Year,
                poster: response.data.Poster
              }
              this.setState({
                movies: [...this.state.movies, filmData]
              })
            })
        })
      })
      .catch(error => console.log(error))
  }

  render() {
    console.log("state", this.state)
    return (
      <div>
        {this.state.movies.map(movie => {
          return (
            <div key={movie.imdbId}>
              <div>
                <img src={movie.poster} alt={movie.imdbId} />
              </div>
              <div>{movie.title}</div>
              <div>{movie.year}</div>
            </div>
          )
        })}
      </div>
    )
  }
}

//component will mount, constructor, componentwillupdate
