import { UserInput, UserOutput, UserLogin } from './user.model'
import { MovieInput, MovieOutput } from './movie.model'

import { getConnection } from './mongoDB'
import bcrypt from 'bcrypt'
import KoaRouter from 'koa-router'
import { Context } from 'koa'
import 'koa-bodyparser'
import { ObjectID } from 'bson'

const routes = new KoaRouter()

// holds all sessions'ids
const sessionMap: any = {}

// route registration
routes.post('/users', async (ctx: Context) => {
  let user: UserInput = ctx.request.body
  let password = user.password

  let nickname = user.nickname
  let email = user.email

  // Check if nickname and email is unique
  let isInDb = async (): Promise<boolean> => {
    const inDb = await db
      .collection('users')
      .findOne({ $or: [{ email: email }, { nickname: nickname }] })
    if (inDb) {
      return true
    }
    return false
  }

  const saltRounds = 10
  const hash = await bcrypt.hash(password, saltRounds)
  // Store hash in your password DB.
  let userHash: UserInput = Object.assign({}, user, { password: hash })
  const db = await getConnection()
  try {
    let userExists = await isInDb()
    if (userExists) {
      ctx.status = 404
      return
    }
    const stuff = await db.collection('users').insertOne(userHash)
    const id: string = stuff.insertedId.toHexString()
    const res: UserOutput = {
      id: id,
      email: user.email,
      gender: user.gender,
      nickname: user.nickname,
    }
    ctx.response.body = res
  } catch (err) {
    console.log(err.stack)
  }
})

// route login
routes.post('/users/login', async (ctx: Context) => {
  const userLogin: UserLogin = ctx.request.body
  let password = userLogin.password
  const saltRounds = 10
  const hash = await bcrypt.hash(password, saltRounds)
  let userHash: UserLogin = Object.assign({}, userLogin, { password: hash })
  const db = await getConnection()
  try {
    const users = await db
      .collection('users')
      .find({ nickname: userHash.nickname })
      .toArray()
    for (let user of users) {
      // console.log(user)
      // console.log(elemen)
      const res = await bcrypt.compare(password, user.password)
      if (res) {
        const sessionId: string = getUniqueId()
        sessionMap[sessionId] = userHash
        ctx.set('Set-Cookie', 'sessionId=' + sessionId)
        ctx.status = 200
        const response: UserOutput = {
          id: user._id,
          email: user.email,
          gender: user.gender,
          nickname: user.nickname,
        }
        ctx.response.body = response
      } else {
        ctx.status = 404
      }
    }
  } catch (err) {}
  console.log('endTime', process.hrtime())
})

//route movies
routes.post('/movies', async (ctx: Context) => {
  const userMovie: MovieInput = ctx.request.body
  const movie = {
    title: userMovie.title,
    imdbId: userMovie.imdbId,
    rating: [],
  }
  const db = await getConnection()
  try {
    const stuff = await db.collection('movies').insertOne(movie)
    const id: string = stuff.insertedId.toHexString()
    const res: MovieOutput = {
      id: id,
      title: movie.title,
      imdbId: movie.imdbId,
      rating: movie.rating,
    }
    ctx.response.body = res
  } catch (err) {}
})
const getUniqueId = (): string => {
  return Math.random().toString()
}

routes.get('/movies/:id', async (ctx: Context) => {
  const id = ctx.params.id
  const db = await getConnection()
  try {
    const res = await db
      .collection('movies')
      .find({ _id: new ObjectID(id) })
      .toArray()
    ctx.response.body = res
    ctx.status = 200
  } catch (err) {
    ctx.status = 404
  }
})

routes.get('/movies', async (ctx: Context) => {
  const db = await getConnection()
  try {
    const res = await db
      .collection('movies')
      .find()
      .toArray()
    ctx.response.body = res
    ctx.status = 200
  } catch (err) {
    ctx.status = 404
  }
})

module.exports = routes
