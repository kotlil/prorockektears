interface UserInput {
  nickname: string;
  gender: number;
  password: string;
  email: string;
}

interface UserOutput {
  id: string;
  nickname: string;
  gender: number;
  email: string;
}

interface UserLogin {
  nickname: string;
  password: string;
}

export { UserInput, UserOutput, UserLogin };
