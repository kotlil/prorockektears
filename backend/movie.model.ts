interface MovieInput {
  title: string;
  imdbId: string;
}

interface MovieOutput {
  id: string;
  title: string;
  imdbId: string;
  rating: Array<object>; // TODO: Implement ratings
}

export { MovieInput, MovieOutput };
