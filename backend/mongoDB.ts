import { MongoClient, Db } from 'mongodb'

const DB_NAME = 'test'
const DB_URL = `mongodb://localhost:27017/${DB_NAME}`

let db: Db

export const getConnection = async () => {
  if (!db) {
    const client = new MongoClient(DB_URL)
    try {
      await client.connect()
      db = client.db(DB_NAME)
    } catch (e) {
      console.error(e)
    }
  }
  return db
}
